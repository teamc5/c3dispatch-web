
var map, map2;

function initMap() {    
    var myOptions = {
        center: {lat:14.315320, lng:121.079586},
        mapTypeId: google.maps.MapTypeId.SATELLITE,
        zoom: 17,
        styles: [{
        featureType: 'poi',
        stylers: [{ visibility: 'on' }]  // Turn off points of interest.
        }, {
        featureType: 'transit.station',
        stylers: [{ visibility: 'off' }]  // Turn off bus stations, train stations, etc.
        }],
        disableDoubleClickZoom: true
    }

    map2 = new google.maps.Map(document.getElementById('map2'), myOptions);
    try{        
        map = new google.maps.Map(document.getElementById('map'), myOptions);
    }catch(error){
        
    }
    var coord = new google.maps.LatLng(14.315320, 121.079586);
    var marker = [];
    var db = firebase.firestore();

    db.collection("responders").get().then(function(querySnapshot) {      
        querySnapshot.forEach(function(doc) {
            marker.push(
                new google.maps.Marker({
                    position: {lat: doc.get('lat'), lng: doc.get('lng')},
                    map: map,
                    title: doc.get('name')
                    })
            );
        });
    
        
    });
    db.collection("responders")
    .onSnapshot(function(querySnapshot) {
        var responders = [];
        querySnapshot.forEach(function(doc) {
            responders.push(doc.data());

        });

        for (i = 0; i < responders.length; i++) {
            marker[i].setPosition({lat: responders[i]['lat'], lng: responders[i]['lng']});
        }
        
    });
}