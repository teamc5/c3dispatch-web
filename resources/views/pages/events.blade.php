@extends('layouts.app')

@section('content')
@if(session()->exists('username'))
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-firestore.js"></script>
<script src="{{ asset('js/firestore.js') }}"></script>
<script src="{{ asset('js/navbar.js') }}"></script>
<script src="{{ asset('js/conversation.js') }}"></script>
<script>

    $(document).on("click", ".reportperson", function(){
        
        var report_id = jQuery(this).attr("id");
        var report_name = jQuery(this).attr("name");
        
        $.get('/sendmessage', function(data){
            ReturnReportDetails(report_id, report_name);
        })

        if(report_name == "incident_reports")
            showModal('reportModal');
        else if(report_name == "patient_assessment")
            showModal('patientAssessmentModal');

    });

    $(document).on("click", "#sendbtn", function(){
         $.get('/sendmessage', function(data){
             var user_details = {!! json_encode($user_details) !!}
            var msg = $('#msgbox').val();
            SendMessage(msg, user_details);
        })
    });

    $(document).on("click", ".eventperson", function(){
        var event_id = jQuery(this).attr("id");
        $.get('/conversations/' + event_id, function(data){
            ReturnEventDetails(data);
            ReturnReports();
        })
    });

    $(document).ready(function(){
        ReadOngoingEvents();
        
        $('.eventperson').trigger("click");
    });
</script>


<div class="container col-lg-12" style="margin-top:2%">
    <div class="contacts col-sm-3">  
        <div>
            <h4>Ongoing Events</h4>        
            <div id="ongoing_div">
                
            </div>
        </div>
        <div>
            <h4>Past Events</h4>        
            {{--  @if(count($names) > 0)
                @foreach($names as $name)
                    <a href="?msg=15" class="chatperson">
                        <span class="chatimg">
                            <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                        </span>
                        <div class="namechat">
                            <div class="pname">{{ $name }}</div>
                            <div class="lastmsg">sdk nskdfjnlssdf sdf ss fsdaf kjlsadjf nkksaddbhk jasddl sdf kjsdfoashf89f sdbfoi nkksaddbhk jasddl</div>
                        </div>
                    </a>
                @endforeach
            @endif  --}}
        </div>
    </div>

    <div class="col-sm-9">
        <div class="row">
            <h3 id="eventh3"></h3>
        </div>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li id="convoTab" class="nav-item">
                <a class="nav-link" id="convo-tab" data-toggle="tab" href="#convo" role="tab" aria-controls="convo" aria-selected="true">Conversation</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="reports-tab" data-toggle="tab" href="#reports" role="tab" aria-controls="reports" aria-selected="false">Reports</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="photos-tab" data-toggle="tab" href="#photos" role="tab" aria-controls="photos" aria-selected="false">Photos</a>
            </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade" id="convo" role="tabpanel" aria-labelledby="convo-tab">
                    <div class="chatbody">
                        <table class="table" id="convo_table">

                        </table>
                    </div>
                
                    <div class="row">
                        <form>
                            <div class="form-group col-xs-9">
                                <input id="msgbox" class="form-control" placeholder="Enter message...">
                            </div>
                            <div class="col-xs-3">
                                <button id="sendbtn" class="btn btn-info btn-block">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane fade" id="reports" role="tabpanel" aria-labelledby="reports-tab">
                    <div>
                        <h4>Reports</h4>
                        <div id="reports_div">                            
                            {{--  <a href="?msg=15" class="chatperson">
                                <span class="chatimg">
                                    <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                                </span>
                                <div class="namechat">
                                    <div class="pname">Patient Information</div>
                                    <div class="lastmsg">ORCAR ARENA 63y.o </br> fr. SAN PEDRO LAGUNA</div>
                                </div>
                            </a>  --}}
                        </div>
                        
                        
                        <button type="button" class="btn btn-dark btn-md btn-block" data-toggle="modal" data-target="#reportsModal">See more reports</button>
            
                    </div>
                </div>
                <div class="tab-pane fade" id="photos" role="tabpanel" aria-labelledby="photos-tab">
                        <div>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            <span class="docuimg">
                                <img src="http://via.placeholder.com/50x50?text=A" alt="" />
                            </span>
                            
                        </div>
                </div>
            </div>
    </div>
    </div>
</div>
@endsection







<!-- PATIENT ASSESSMENT -->
<div class="modal fade" id="patientAssessmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <h3>Patient Assessment</h3>
                <div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="pLocation">Time of Call</label>
                            <input type="text" class="form-control" id="TimeOfCall" placeholder="Time of Call">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="pLastName">Name</label>
                            <input type="text" class="form-control" id="pLastName" placeholder="Patient's Last Name">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="pFirstName">Name</label>
                            <input type="text" class="form-control" id="pFirstName" placeholder="Patient's First Name">
                        </div>
                    </div>
                    <div class="form-row">   
                        <div class="form-group col-md-4">
                            <label for="pGender">Gender</label>
                            <select id="pGender" class="form-control">
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>                         
                        
                    </div>
                </div>

                <h3>Patient Profile</h3>
                <div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="pName">Name</label>
                            <input type="text" class="form-control" id="pName" placeholder="Patient's Name">
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label for="pAddress">Address</label>
                            <input type="text" class="form-control" id="pAddress" placeholder="Patient's Address">
                        </div>

                        <div class="form-group col-md-12">
                                <label for="pLocation">Location</label>
                                <input type="text" class="form-control" id="pLocation" placeholder="1234 Main St">
                            </div>
                    </div>
                    <div class="form-row">
                        

                        <div class="form-group col-md-4">
                            <label for="pAge">Age</label>
                            <input type="text" class="form-control" id="pAge" placeholder="Patient's Age">
                        </div>

                        <div class="form-group col-md-4">                                
                        </div>
                    </div>
                    <div class="form-row">                            
                        <div class="form-group col-md-12">
                            <label for="pAssessment">Initial Assessment</label>
                            <input type="text" class="form-control" id="pAssessment" placeholder="Initial Assessment">
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Return to Conversation</button>  
            </div>
        </div>
    </div>
</div>


<!-- EXTEND REPORTS MODAL -->
<div class="modal fade" id="reportsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        <div class="modal-body">
            <h3>Reports</h3>
            <div class="list-group">
                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                    <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Injury Report</h5>
                    <small>3 minutes ago</small>
                    </div>
                    <p class="mb-1">JOYCE ANN BURRATA 32y.o fr. CABILANG BAYBAY CARMONA C</p>  
                    <small class="text-muted"> Reported by Archie Alcain</small>                  
                </a>
                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Vehicular Accident</h5>
                    <small class="text-muted">10 minutes ago</small>
                    </div>
                    <p class="mb-1">ORCAR ARENA 63y.o fr. SAN PEDRO LAGUNA</p>
                    <small class="text-muted"> Reported by Danica Nacionales</small>             
                </a>
                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                    <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Patient Information</h5>
                    <small class="text-muted">12 minutes ago</small>
                    </div>
                    <p class="mb-1">JOYCE ANN BURRATA 32y.o fr. CABILANG BAYBAY CARMONA C</p>
                    <small class="text-muted">Reported by Aaron Quijano</small>
                </a>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Return to Conversation</button>  
        </div>
        </div>
    </div>
</div>

<!-- INCIDENT REPORT -->
<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <h3>Incident Report</h3>
                <div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="iDate">Date</label>
                            <input type="text" class="form-control" id="iDate" placeholder="Date">
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label for="iTimeOfCall">Time of Call</label>
                            <input type="text" class="form-control" id="iTimeOfCall" placeholder="Time of Call">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="iCallReceiver">Call Receiver</label>
                            <input type="text" class="form-control" id="iCallReceiver" placeholder="Call Receiver">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="iTimeOfArrival">Time of Arrival at the Location</label>
                            <input type="text" class="form-control" id="iTimeOfArrival" placeholder="Time of Arrival at Location">
                        </div>
                    </div>
                    <div class="form-row">                            
                        <div class="form-group col-md-12">
                            <label for="iLocation">Location</label>
                            <input type="text" class="form-control" id="iLocation" placeholder="1234 Main St">
                        </div>
                    </div>
                </div>

                <h3>Patient Profile</h3>
                <div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="iName">Name</label>
                            <input type="text" class="form-control" id="iName" placeholder="Patient's Name">
                        </div>
                        
                        <div class="form-group col-md-6">
                            <label for="iAddress">Address</label>
                            <input type="text" class="form-control" id="iAddress" placeholder="Patient's Address">
                        </div>

                        
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="iGender">Gender</label>
                            <select id="iGender" class="form-control">
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="iAge">Age</label>
                            <input type="text" class="form-control" id="iAge" placeholder="Patient's Age">
                        </div>

                        <div class="form-group col-md-4">                                
                        </div>
                    </div>
                    <div class="form-row">                            
                        <div class="form-group col-md-12">
                            <label for="iAssessment">Initial Assessment</label>
                            <input type="text" class="form-control" id="iAssessment" placeholder="Initial Assessment">
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Return to Conversation</button>  
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="photosModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
        
        <div class="modal-body">
            <h3>Photos</h3>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Return to Conversation</button>  
        </div>
        </div>
    </div>
</div>


<script src="{{ asset('js/map.js') }}"></script>
@endif