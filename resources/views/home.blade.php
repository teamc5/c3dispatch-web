@extends('layouts.app')

@section('content')
@if(session()->exists('username'))
    <div class="container col-lg-12">
        <div class="col-lg-6">
            <div class="row">
                <h1 class="display-1">Dashboard</h1>
            </div>
            <div class="row">
                <div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Dashboard</div>
                        
                        <div class="panel-body">
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                    
                                    You are logged in!
                                </div>

                            @if(count($responders) > 0)
                                @foreach($responders as $responder)
                                    <p> {{ $responder['name'] }}</p>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-heading">Responder Locations</div>
    
                        <div class="panel-body" >
                            <img src="{{ asset('img/map2.png') }}" class="img-fluid" style="max-width:100%" alt="Responsive image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endif
@endsection

<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.9.1/firebase-firestore.js"></script>
<script src="{{ asset('js/firestore.js') }}"></script>
<script src="{{ asset('js/map.js') }}"></script>