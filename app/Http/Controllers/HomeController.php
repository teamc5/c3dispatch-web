<?php

namespace DSPTCH\Http\Controllers;

use Illuminate\Http\Request;
use DSPTCH\Users;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$responders = Users::all();
        return view('home');
        //->with('responders', $responders);
    }
}
