<?php namespace DSPTCH\Providers;

use View;
use Illuminate\Support\ServiceProvider;
use DSPTCH\Users;

class ComposerServiceProvider extends ServiceProvider {

    public function boot()
    {
        // View::composer('layouts.app', 'App\Http\ViewComposers\ProfileComposer');

        View::composer('layouts.app', function($view){
            $responders = Users::all();
            $view->with('responders', $responders);
        });
    }


    public function register()
    {
        //
    }
}