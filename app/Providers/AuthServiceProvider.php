<?php

namespace DSPTCH\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use DSPTCH\Http\Controllers\Auth\FirestoreAuthGuard;
use DSPTCH\Database\FirestoreDB;
use DSPTCH\FirestoreUser;
use DSPTCH\Providers\FirestoreAuthServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'DSPTCH\Model' => 'DSPTCH\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        
        //
        $this->app->bind('DSPTCH\Database\FirestoreDB', function ($app) {
          return new FirestoreDB();
        });
         
        $this->app->bind('DSPTCH\FirestoreUser', function ($app) {
          return new FirestoreUser();
        });
        // add custom guard provider
        Auth::provider('firestore', function ($app, array $config) {
          return new FirestoreAuthServiceProvider($app->make('DSPTCH\FirestoreUser'));
        });
     
        // add custom guard
        Auth::extend('json', function ($app, $name, array $config) {
          return new FirestoreAuthGuard(Auth::createUserProvider($config['provider']), $app->make('request'));
        });
    }
}
