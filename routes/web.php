<?php


use DSPTCH\UserModel;
use DSPTCH\Drive;
use DSPTCH\Database\FirestoreDB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('phpinfo', function() {

    echo phpinfo();
});


Route::get('/login', 'FirestoreAuthController@login');
Route::get('/regacc', 'FirestoreAuthController@register');
Route::get('/register', 'PagesController@register');
Route::get('/usertest', 'PagesController@getUsers');
Route::get('/fbase', function() {
    return view('firebase')->with('token', session() -> get('token'));

});

Route::get('/testt', function() {
    return base_path().'\rsaprivate.pem';

});



Route::group(['middleware' => ['authcheck']], function () {
    Route::get('/home', 'PagesController@home')->name('home');
    Route::get('/dispatcherslocation', 'PagesController@dispatchers_location');
    Route::get('/notifications', 'PagesController@notifications');
    Route::get('/settings', 'PagesController@settings');

    Route::get('/conversations', 'PagesController@conversations');
    Route::get('/conversations/{event_id?}', function($event_id){
        return $event_id;
    });    
    Route::get
    ('/sendmessage', function(){
        return "haaa";
    });


    Route::get('/getusers', 'NavController@GetResponders');

    Route::get('/logout', function () {
        session() -> flush();
        return view('welcome');
    });
});

Route::get('/testexcel', function() {
    $excel = new DSPTCH\Excel();
    $excel -> setWorksheetMonth(1);
    $test = [
        
        'date' => 'test',
        'shift' => 'test',
        'city' => 'test',
        'incident_place' => 'test',
        'incident_monitored' => 'test',
        'other_services' => 'test',
        'injured' => 'test',
        'missing' => 'test',
        'dead' => 'test',
        'weather'  => 'test',
        'remarks' => 'test'
    ];

    $excel -> writeReport($test);
    //return $excel -> getCurrentCell(11);
});

Route::get('/sessioncheck', function () {
    return session()->get ('username');
});

Route::get('/sessiondestroy', function() {
    session()->flush();
    return view('welcome');
});

Route::get('/hash', function(){
    return Hash::make('test');
});

Route::get('/userdetails', function() {
    $firestore = new FirestoreDB();

        $user = $firestore -> get_document('Responders', ['user_id', '=', session() -> get('username')]);
        $user_details = [];

        foreach ($user as $details) {
            array_push($user_details, [
                    'location' => $details['location'],
                    'status' => $details['status'],
                    'name' => $details['username']
                ]);
        }

        return $user_details;
    });


Route::get('/pushertest', function() {
    $options = array(
        'cluster' => 'ap1',
        'encrypted' => true
    );
    $pusher = new Pusher\Pusher(
        '5d2bc6d714fd628bcd30',
        '5fa44d03de782d0e78a8',
        '482817',
        $options
    );

    $data['message'] = 'hello world';
    $pusher->trigger('my-channel', 'my-event', $data);

});

