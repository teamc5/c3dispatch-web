var map;
var marker, marker2;
var db = firebase.firestore();

function initMap() {
    var myOptions = {
        center: {lat:14.315320, lng:121.079586},
        mapTypeId: google.maps.MapTypeId.HYBRID,
        zoom: 17,
        styles: [{
            featureType: 'poi',
            stylers: [{ visibility: 'on' }]  // Turn off points of interest.
        }, {
            featureType: 'transit.station',
            stylers: [{ visibility: 'off' }]  // Turn off bus stations, train stations, etc.
        }],
        disableDoubleClickZoom: true
    }

    map2 = new google.maps.Map(document.getElementById('map2'), myOptions);
    try{        

      //DISPATCHERS LOCATION
        map = new google.maps.Map(document.getElementById('map1'), myOptions);

        // get places auto-complete when user type in location-text-box
        var input = document.getElementById('location-text-box1');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29),
            draggable: true
        });

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
            return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
            } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17); // Why 17? Because it looks good.
            }
            marker.setIcon( /** @type {google.maps.Icon} */ ({
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(35, 35)
            }));
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            var address = '';
            if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
            }
            document.getElementById("lat").value = place.geometry.location.lat();
            document.getElementById("long").value = place.geometry.location.lng();
        });

        google.maps.event.addListener(map, 'click', function(event) {
            document.getElementById("lat").value = event.latLng.lat();
            document.getElementById("long").value = event.latLng.lng();
            document.getElementById('location-text-box1').value = "";
            marker.setPosition(event.latLng);
          });


        
    }catch(error){
      console.log(error.message);
    }


  

//--------------------------------------------------------------------------------


// var mapOptions = {
//     zoom: 12
//   };
//   map = new google.maps.Map(document.getElementById('map2'),
//     mapOptions);

// get places auto-complete when user type in location-text-box

//ADD EVENT MAP
try{
  var input = document.getElementById('location-text-box');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map2);

    var infowindow = new google.maps.InfoWindow();
    marker2 = new google.maps.Marker({
    map: map2,
      anchorPoint: new google.maps.Point(0, -29),
      draggable: true
    });

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    infowindow.close();
    marker2.setVisible(false);
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    // If the place has a geometry, then present it on a map.
    if (place.geometry.viewport) {
      map2.fitBounds(place.geometry.viewport);
    } else {
      map2.setCenter(place.geometry.location);
      map2.setZoom(17); // Why 17? Because it looks good.
    }
    marker2.setIcon( /** @type {google.maps.Icon} */ ({
      url: place.icon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(35, 35)
    }));
    marker2.setPosition(place.geometry.location);
    marker2.setVisible(true);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''), (place.address_components[1] && place.address_components[1].short_name || ''), (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }
    document.getElementById("lat2").value = place.geometry.location.lat();
    document.getElementById("long2").value = place.geometry.location.lng();
  });
}catch(error){
  console.log(error.message);
}





var responderMarker = [];
db.collection("Responders").get().then(function(querySnapshot) {

  querySnapshot.forEach(function(doc) {
      // doc.data() is never undefined for query doc snapshots

      var responder_loc = {'username': doc.data().username, 
        'lat': doc.data().location.latitude,
        'lng': doc.data().location.longitude
      };

        marker = new google.maps.Marker({
          position: {lat: responder_loc.lat, lng: responder_loc.lng},
          map: map,
          title: responder_loc.username
      });
        responderMarker.push(marker);
      });
});


db.collection("Responders").onSnapshot(function(querySnapshot) {
    querySnapshot.forEach(function(doc) {

        lat = doc.data().location.latitude;
        lng = doc.data().location.longitude;

        function findResponder(responders) {
          return responders.getTitle() == doc.data().username;
        }
        var idx = responderMarker.findIndex(findResponder);
        responderMarker[idx].setPosition(new google.maps.LatLng( lat, lng ));
    });
});


  
  // var service = new google.maps.places.PlacesService(map2);
  // google.maps.event.addListener(map2, 'click', function(event) {
  //   document.getElementById("lat2").value = event.latLng.lat();
  //   document.getElementById("long2").value = event.latLng.lng();
  //   // document.getElementById('location-text-box').value = "";
  //   marker2.setPosition(event.latLng);

  //   var infowindow = new google.maps.InfoWindow();
  //   service.nearbySearch({
  //     location: { lat: event.latLng.lat(), lng: event.latLng.lng() },
  //     radius: 200,
  //     type: ['establishment']
  //   }, callback);  

  // });

  // function callback(results, status) {  
  //   var place = results[0];

  //   if (status === google.maps.places.PlacesServiceStatus.OK){
  //     // document.getElementById('location-text-box').value = place.name;
  //   }

  //   for(var i = 0; i < results.length; i++){
  //     var placeLoc = place.geometry.location;
  //     var marker = new google.maps.Marker({
  //       map: map2,
  //       position: place.geometry.location
  //     });

  //     google.maps.event.addListener(marker, 'click', function(){
  //       infowindow.setContent(place.name);
  //       infowindow.open(map2, this);
  //     })
  //   }

  // }
  







  // if(status != google.maps.places.PlacesServiceStatus.OK)
  // if(results.length < 1) return;

  // var request = {
  //   location: { lat: document.getElementById("lat2").value, lng: document.getElementById("long2").value },
  //   radius: 200,
  //   type: ['store']
  // };

  // service.nearbySearch(request, function(){
  //   document.getElementById('location-text-box').value = ""
  // })






}