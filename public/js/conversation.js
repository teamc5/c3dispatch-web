var db = firebase.firestore();

var selectedEvent_id;

var ReadOngoingEvents = function(){
    var d1 = document.getElementById('ongoing_div');
    db.collection("Events").where("status", "==", "Ongoing").onSnapshot(function(querySnapshot){    

        querySnapshot.docChanges.forEach(function(change){
            if(change.type === "added"){
                d1.insertAdjacentHTML('beforeend', '<a href="#" id="'+change.doc.id+'" class="eventperson"><span class="eventimg"><img src="http://via.placeholder.com/50x50?text=A" alt="" /></span><div class="nameevent"><div class="pname">'+change.doc.data().eventName+'</div><div class="lastmsg">'+change.doc.data().location+'</div></div></a>');                
            }
            if(change.type === "removed"){
                document.getElementById(change.doc.id).remove();
            }
        })
    });
}

function ReturnEventDetails(event_id){
    if(event_id != selectedEvent_id){
        $("#convo_table").html("");
        selectedEvent_id = event_id;

        var convoRef = db.collection("Events").doc(event_id).collection("Conversation");
        var respRef = db.collection("Responders");
        
        db.collection("Events").doc(event_id).get().then(snap =>{        
            $("#eventh3").html(snap.data().eventName);
        });
        

        convoRef.orderBy("timestamp").onSnapshot(function(querySnapshot){
            var d1 = document.getElementById('convo_table');
            querySnapshot.docChanges.forEach(function(change){ //for each message
                if(change.type === "added"){
                    var date = new Date(change.doc.data().timestamp);
                    console.log((date.getMonth() + 1) + " " + date.getDay() + ", " + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes());
                    d1.insertAdjacentHTML('beforeend', '<tr><td><img src="http://via.placeholder.com/50x50?text=A" /></td><td>'+change.doc.data().sender+'</td><td>'+change.doc.data().message+'</td><td>'+date.getHours() + ":" + date.getMinutes()+'</td></tr>');                    
                }
            });
        });
    }
}

function SendMessage(message,user_details){
    var convoRef = db.collection("Events").doc(selectedEvent_id);

    convoRef.collection("Conversation").add({
        message: message,
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        sender:  user_details["name"] // change this to the user's logged in
    }).then(function(){
        console.log("Message Sent!");
    }).catch(function(error){
        console.error("Error: " + error);
    })
}

var reports = [];
var reports_id = [];
var patientAssess = [];
var patientAssess_id = [];

function ReturnReports(){
    var d1 = document.getElementById('reports_div');
    $("#reports_div").html("");
    var incidentRef = db.collection("Events").doc(selectedEvent_id).collection("Incident Reports");

    incidentRef.onSnapshot(function(querySnapshot){
        querySnapshot.docChanges.forEach(function(change){
            if(change.type === "added"){
                reports.push(change.doc.data());
                reports_id.push(change.doc.id);
                d1.insertAdjacentHTML('beforeend', '<a href="#" name="incident_reports" id="'+change.doc.id+'" class="reportperson"><span class="reportimg"><img src="http://via.placeholder.com/50x50?text=A" alt="" /></span><div class="namereport"><div class="pname">Incident Report</div><div class="lastmsg">'+change.doc.data().patientName+' </br> fr. '+change.doc.data().patientAddress+'</div></div></a>');
            }
            if(change.type === "removed"){
                document.getElementById(change.doc.id).remove();
            }
        })
    });

    var patientRef = db.collection("Events").doc(selectedEvent_id).collection("Patient Assessments");

    patientRef.onSnapshot(function(querySnapshot){
        querySnapshot.docChanges.forEach(function(change){
            if(change.type === "added"){
                patientAssess.push(change.doc.data());
                patientAssess_id.push(change.doc.id);
                d1.insertAdjacentHTML('beforeend', '<a href="#" name="patient_assessment" id="'+change.doc.id+'" class="reportperson"><span class="reportimg"><img src="http://via.placeholder.com/50x50?text=A" alt="" /></span><div class="namereport"><div class="pname">Patient Assessment</div><div class="lastmsg">'+change.doc.data().patientGivenName+' '+change.doc.data().patientFamilyName+' </br> fr. '+change.doc.data().patientAddress+'</div></div></a>');
            }
            if(change.type === "removed"){
                document.getElementById(change.doc.id).remove();
            }
        })
    });

}

function ReturnReportDetails(report_id, report_name){
    for(var i=0; i < reports_id.length; i++){
        if(reports_id[i] == report_id){

            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

            var date = new Date(reports[i].date);
            var timeOfCall = new Date(reports[i].timeOfCall);
            var timeOfArrival = new Date(reports[i].timeOfArrival);

            document.getElementById("iDate").value = monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();
            document.getElementById("iTimeOfCall").value = ('0' + timeOfCall.getHours()).slice(-2) + ":" + ('0' + timeOfCall.getMinutes()).slice(-2);
            document.getElementById("iCallReceiver").value = reports[i].callReceiver;
            document.getElementById("iTimeOfArrival").value = ('0' + timeOfArrival.getHours()).slice(-2) + ":" + ('0' + timeOfArrival.getMinutes()).slice(-2);
            document.getElementById("iLocation").value = reports[i].location;

            document.getElementById("iName").value = reports[i].patientName;
            document.getElementById("iAddress").value = reports[i].patientAddress;
            document.getElementById("iGender").value = reports[i].patientGender;
            document.getElementById("iAge").value = reports[i].patientAge;
            document.getElementById("iAssessment").value = reports[i].patientIniAssess;
        }
    }
}